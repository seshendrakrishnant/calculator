public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "My Calculator App!" );
        System.out.println( "Adding two numbers 3 and 4!" );
        int res = add(3,4);
        System.out.println( "Result of addition is " + res);
    }

    public static int add(int a, int b) {
	return a+b;
    }
}
